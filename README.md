# EXAALT - EXascale Atomistics for Accuracy, Length and Time

## EXAALT Group/Repo Quick Intro
The `exaalt` repository is intended to contain the following components:
* EXAALT-specific code for running the one or all of the following software packages together: LAMMPS, ParSplice and LATTE
* Metadata for testing and building the EXAALT framework
* A `lammps` submodule (Meaning: a reference to a specific commit of a separate LAMMPS repository). Currently, this submodule references the `lammps` repository in this exaalt group. For now, this separate repository is setup to mirror (synchronize with) the LAMMPS github repository.
* A `LATTE` submodule (Meaning: a reference to a specific commit of a separate LATTE repository). Currently, this submodule references the `LATTE` repository in this exaalt group.
* A `parsplice` submodule (Meaning: a reference to a specific commit of a separate ParSplice repository). Currently, this submodule references the `parsplice` repository in this exaalt group.

## SEE WIKI (https://gitlab.com/exaalt/exaalt/wikis/home) FOR MORE INFO/INSTRUCTIONS
