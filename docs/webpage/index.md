---
layout: default
title: {{ site.name }}
---


# EXAALT - EXascale Atomistics for Accuracy, Length and Time

## EXAALT Quick Intro
The `exaalt` computational framework involves the following components:

- EXAALT-specific code for running the one or all of the following software packages together: LAMMPS, ParSplice and LATTE

- Metadata for testing and building the EXAALT framework

- A `lammps` submodule (Meaning: a reference to a specific commit of a separate LAMMPS repository). Currently, this submodule references the `lammps` repository in this exaalt group. For now, this separate repository is setup to mirror (synchronize with) the LAMMPS github repository.

- A `LATTE` submodule (Meaning: a reference to a specific commit of a separate LATTE repository). Currently, this submodule references the `LATTE` repository in this exaalt group.

- A `parsplice` submodule (Meaning: a reference to a specific commit of a separate ParSplice repository). Currently, this submodule references the `par splice` repository in this exaalt group. However, the referenced repository is empty, because there is an *export control* issue that needs to be resolved before the source code is populated.

## Building the components:

[PARSPLICE](./parsplice/index.html)

[LAMMPS](http://lammps.sandia.gov/)

[LATTE](https://github.com/lanl/LATTE/blob/master/README.md)


## LAMMPS-LATTE tutorial:

A step-by-step tutorial with an example on how to run a LAMMPS-LATTE
calculation.

[LAMMPS-LATTE](./tutorial-lammps-latte.pdf)

[LAMMPS-LATTE](./tutorial.html)
