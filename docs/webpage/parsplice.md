---
layout: subpage
title: Parsplice
permalink: /parsplice.html
---

# Building ParSplice 

`mkdir build`

`cd build`

Configure (Using a lammps installation WITHOUT latte):

`cmake -DCMAKE_INCLUDE_PATH=${PREFIX}/include/
-DCMAKE_LIBRARY_PATH=${PREFIX}/lib -DCMAKE_CXX_COMPILER=${COMPILER} ../`

Configure (Using a lammps installation WITH latte):

`cmake -DCMAKE_INCLUDE_PATH=${PREFIX}/include/
-DEXTRA_FLAGS="-I${latte_PATH}/src" -DEXTRA_LINK_FLAGS="-fopenmp
${latte_PATH}/src/latte_c_bind.o ${latte_PATH}/liblatte.a -lgfortran  -llapack
-lblas" -DCMAKE_LIBRARY_PATH=${PREFIX}/lib -DCMAKE_CXX_COMPILER=${COMPILER}
../`

Make:

`make`



## Dependencies

*  Choose a PREFIX where to install the dependencies (i.e., ${HOME}/local).
*  Get sources from `ParSplice-deps/`
*  Compile and install as follows


### Boost:
Bootstrap to set your install prefix:

`./bootstrap.sh --prefix=${PREFIX}`

Build with gcc and c++11 support:

`./b2 toolset=gcc cxxflags=-std=c++11`

Install:

`./b2 install`


### Berkeley DB

` cd build_unix`

Configure:

`../dist/configure --enable-stl --enable-cxx --prefix=${PREFIX}`

Make:

`make`

Install:

`make install`


### Eigen

`cp -r Eigen ${PREFIX}/include`


### Nauty

Configure: 
`./configure --prefix=${PREFIX}`

Make:

`make`

Install:

`cp nauty.a ${PREFIX}/lib/libnauty.a`

`mkdir ${PREFIX}/include/nauty`

`cp *.h ${PREFIX}/include/nauty`


### LAMMPS

Configure your LAMMPS 

`make yes-manybody`

`make yes-kspace`

`make `yes-snap`

`make `yes-misc`

etc...


Edit the makefile

Make:

`make mpi`

Make the library:

`make mpi mode=lib`


Install:

`cp liblammps_mpi.a ${PREFIX}/lib`

`mkdir ${PREFIX}/include/lammps`

`cp *.h  ${PREFIX}/include/lammps/`

