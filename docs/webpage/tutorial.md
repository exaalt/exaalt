---
layout: page
title: Tutorial lammps-latte
permalink: /tutorial.html
---
 
![image](./arte.png){width="10cm"}

Introduction
============

This notes are intended to give a step-by-step “howto” run a Density
Functional Tight-Binding (DFTB) based Quantum Molecular Dynamics
([QMD](https://arxiv.org/abs/1203.6836)) calculation using
[LAMMPS](https://github.com/lammps/lammps) and
[LATTE](https://github.com/lanl/LATTE) computational codes. Large-scale
Atomic/Molecular Massively Parallel Simulator (LAMMPS) is an intensively
used molecular dynamics code written in C++@PLIMPTON19951. Los Alamos
Transferable Tight-binding for Energetics (LATTE) is a LANL Fortran code
used for quantum chemistry based on the tight-binding method
@Sanville2010. The LAMMPS documentation can be found in
[`http://lammps.sandia.gov/doc/Manual.html`](http://lammps.sandia.gov/doc/Manual.html).
The LATTE documentation can be found in
[`https://github.com/lanl/LATTE/blob/master/Manual/LATTE_manual.pdf`](https://github.com/lanl/LATTE/blob/master/Manual/LATTE_manual.pdf).
With the help of an interface we are now able to drive a molecular
dynamics (MD) simulation with LAMMPS using the forces computed at the
DFTB level of theory with LATTE.

Forces are calculated by LATTE from the electronic structure of the
system:

$$F_k = -tr \left[\rho \frac{\partial H}{\partial R_k} \right] - \frac{1}{2} \sum_i \sum_{j \neq i} q_i q_j \frac{\partial \gamma_{ij}}{\partial R_k} 
    - \frac{\partial E_{pair}}{\partial R_k}$$

Requirements
============

In order to follow this tutorial, we will assume that the reader have a
`LINUX` or `MAC` operative system with the following packages properly
installed:

-   The `git` program for cloning the codes.

-   A `C/C++` compiler (`gcc` and `g++` for example)

-   A `Fortran` compiler (`gfortran` for example)

-   The LAPACK and BLAS library (GNU `libblas` and `liblapack`
    for example)

-   The `python` interpreter (not essential).

-   The `pkgconfig` and `cmake` programs (not essential).

On an `x86_64` GNU/Linux Ubuntu 16.04 distribution the commands to be
typed are the following:

          $ sudo apt-get update   
          $ sudo apt-get --yes --force-yes install gfortran gcc g++
          $ sudo apt-get --yes --force-yes install libblas-dev liblapack-dev   
          $ sudo apt-get --yes --force-yes install cmake pkg-config cmake-data 
          $ sudo apt-get --yes --force-yes install git python             
      

**NOTE:** Through the course of this tutorial we will assume that the
follower will work and install the programs in the home directory
(`$HOME`).

Download and installation
=========================

Download:
---------

We will need to clone two essential repositories. One for LAMMPS:

          $ cd; git clone -b exaalt https://gitlab.com/exaalt/lammps.git
        

And another for LATTE:

          $ cd; git clone git@github.com:lanl/LATTE.git
        

**NOTE:** Make sure your public ssh key is added to your github profile
in order to be able to clone this repositories. Alternative to the git
command, these two codes can be Downloaded directly as a `.zip` file
from the github webpage clicking on the “clone or download button” and
choosing the “download zip” option. For the case of LAMMPS go to:
[`https://github.com/lammps/lammps`](https://github.com/lammps/lammps)
and for LATTE go to:
[`https://github.com/lanl/LATTE`](https://github.com/lanl/LATTE).

Compiling LATTE:
----------------

The first code that must be compiled is the LATTE code because it has to
be compiled as a library so that it can be linked with LAMMPS. In order
to compile the LATTE code we proceed as following. We enter the LATTE
directory (`cd ~/LATTE`) and modify the `makefile.CHOICES` file
according to the operative system, libraries, compiler, etc. There are
several examples of `makefile.CHOICES` files inside the makefiles
directory (`~/LATTE/makefiles`) that could be used as a template to
replace the `makefile.CHOICES` that is located in the main directory.
**IMPORTANT:** Make sure that the `MAKELIB` variable is set ot `ON`
(`MAKELIB = ON`) to be able to compile LATTE as a library.

If you are running on `x86_64` GNU/Linux Ubuntu 16.04 with the packages
that we suggested to install, then the actual `makefile.CHOICES` should
work.

To build the code just type make inside the main directory as follows:

          $ cd; cd ~/LATTE
          $ make; make test
        

The latter should build the code and test it with some examples that are
located in `~/LATTE/tests`. A file called `liblatte.a` should appear
after building the code. **NOTE:** Make sure to remove the `liblatte.a`
any time the code has to be rebuilt.

Compiling LAMMPS:
-----------------

To build the LAMMPS code and link it with LATTE, got to the LAMMPS
folder and enter in the `lib/latte` directory (`cd ~/lammps/lib/latte`).
Copy one of the Makefiles to Makefile.lammps as follows:
`cp Makefile.lammps.gfortran Makefile.lammps` and edit the
`Makefile.lammps` file according to your system settings, compilers and
LATTE location. There are more instructions in the README file inside
`~/lammps/lib/latte`. After this is done go to the source folder
(`cd ~/lammps/src`) and type the following series of commands:

          $ cd ~/lammps/src
          $ make yes-latte
          $ make yes-molecule 
          $ make serial
        

Running an example calculation
------------------------------

We will assume you have a `pdb/xyz` file containing the coordinates of
your system. For the purpose of this tutorial we will use a simple
system consisting on eight water molecules in a box.

We will make a new directory to be able to run so that we can separate
the running files from program files.

          $ mkdir ~/example_water
          $ cd ~/example_water
          $ cp ~/LATTE/example_lmp/wat_24.pdb . 
        

Now we need to transform the coordinates to the LAMMPS data file which
is formated as follows:

       
     LAMMPS Description
     
              24 atoms
     
               2 atom types
     
       0.0000000000000000        6.2670000000000003      xlo xhi
       0.0000000000000000        6.2670000000000003      ylo yhi
       0.0000000000000000        6.2670000000000003      zlo zhi
     
     Masses
     
                  1   15.994915008544922     
                  2   1.0078250169754028     
     
     Atoms
     
        1    1    1   0.0   3.08800   3.70000   3.12400
        2    1    2   0.0   4.05800   3.70000   3.12400
        3    1    2   0.0   2.76400   3.13200   3.84100
        4    1    1   0.0   2.47000   0.39000   1.36000    
        
        

The format of the file is self explanatory but one can consult the
LAMMPS data format for more information:
[`http://lammps.sandia.gov/doc/2001/data_format.html`](http://lammps.sandia.gov/doc/2001/data_format.html).
For this example we are providing an already formated file which is
located in:\
`~/LATTE/example_lmp/data.wat_24.lmp`

If a particula mass needs to be changed, this has to be done, not only
on the .lmp file, but also inside the “periodic table”

There is a tool that can transform pdb to lmp formats back and forth
which is part of the PROGRESS linbrary located in:
[`https://github.com/lanl/qmd-progress`](https://github.com/lanl/qmd-progress)
If installed, a tool can be used to transform coordinates as follows:

          $ ~/qmd-progress/build/changecoords mycoords.pdb mycoords.lmp
        

There is another tool in
[openbabel](http://open-babel.readthedocs.io/en/latest/FileFormats/The_LAMMPS_data_format.html)
library that can convert from any format to the lammps format but it
cannot convert back from lammps to any other format. See source
[code](https://sourceforge.net/p/openbabel/code/HEAD/tree/openbabel/trunk/src/formats/lmpdatformat.cpp).

A general LAMMPS input file will have to be created. An example input
file can be found in `~/LATTE/example_lmp/in.wat.lmp`. This file has to
be copied to the running folder to be used in this tutorial.

          $ cp ~/LATTE/example_lmp/in.wat.lmp ~/example_water/
        

For a precise definition of the keywords here used please refer to:\
[`http://lammps.sandia.gov/doc/Manual.html`](http://lammps.sandia.gov/doc/Manual.html).
There are some important points to notice:

-   `units` must be set to `metal`

-   `read_data` must be followed by the name of our data file. In this
    case this is: `data.wat_24.lmp`.

-   `atom_modify` must be followed by `sort 0 0.0` in order to avoid an
    error from resorting the coordinates.

-   `fix 2 all latte NULL` will call the LATTE program to compute
    the forces.

The following step is to setup the LATTE environment of input files and
folders that will allow us to run the LAMMPS-LATTE calculations. For
this we can just call a script from the running folder as follows:

          $ cd ~/example_water
          $ ~/LATTE/run_latte --setup  
        

This will automatically make all the necessary folders and input files
used by latte. There are two important files that controls the latte
programs these are `MDcontroller` and `TBparam/control.in `. A detailed
Description of all these keywords can be found in:\
[`https://github.com/lanl/LATTE/blob/master/Manual/LATTE_manual.pdf`](https://github.com/lanl/LATTE/blob/master/Manual/LATTE_manual.pdf)

An alternative way to provide latte with the necessary keywords is to
use a “latte.in” file inside the running folder. If this file is present
latte will read the keywords from there. We provide and example file in
`~/LATTE/example_lmp/latte.in`. Copy this file to the running folder as
follows:

          $ cd ~/example_water
          $ cp ~/LATTE/example_lmp/latte.in .
        

Finally, to run the calculation we just need to type the following
lines:

          $ cd ~/example_water
          $ ~/lammps/src/lmp_serial < in.wat
        

Contacts:
=========

-   For problems with LAMMPS please contact Steve Plimpton, email:
    sjplimp@sandia.gov

-   For problems with LAMMPS-LATTE interface please contact Christian
    Negre, email: cnegre@lanl.gov

-   For problems with LATTE please contact Marc Cawkwell, email:
    cawkwell@lanl.gov


