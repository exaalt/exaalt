---
layout: subpage
title: Howto add!
permalink: /README.html
---

WEBPAGE EXAMPLE FOR EXAALT
==========================

To compile and test the page the following needs to be installed:

    $ sudo apt-get install ruby ruby-dev jekyll

    $ sudo gem install bundler

To install the components for the webpage:

    $ sudo make install (Done only once)

To compile the webpage:

    $ make

To compile and test:

    $ make test

Explore the IP address to visualize the result of the webpage compilation.

parsplice.md shows an example of how we can add a page.


LATEX TO MD
===========

It is possible to convert `.tex` files to `.md` files by using `pandoc` 

    $ sudo apt-get install pandoc 

    $ pandoc mytex.tex -o mymd.md  
