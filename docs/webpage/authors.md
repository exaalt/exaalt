---
layout: subpage
title: Authors
permalink: /authors.html
---

## Authors

(in alphabetical order)

- Aidan Thompson <<aidanthompson@sandia.gov>>
- Arthur Voter <<afv@lanl.gov>>
- Christian F. A. Negre <<cnegre@lanl.gov>>
- Danny Perez <<dannyperez@lanl.gov>>
- Marc J. Cawkwell <<cawkwell@lanl.gov>>
- Steve Plimpton <<sjplimp@sandia.gov>>
- Anders M. N. Niklasson <<amn@lanl.gov>>
- Tim German <<tcgerman@lanl.gov>>
- Richard Zamora <<rjzamora@lanl.gov>>

Los Alamos National Laboratory and Sandia National Laboratory.  
