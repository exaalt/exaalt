\documentclass[12pt]{article}
\usepackage{cite}
\usepackage{graphicx}
\usepackage{amsmath} 
\usepackage{amssymb} 
\usepackage{hyperref}
\usepackage[makeroom]{cancel}
\usepackage{algorithm,algpseudocode}
\usepackage{pgf}
\usepackage{tikz}
\usepackage{authblk}
\usepackage{soul} % use this (many fancier options)
\usepackage{amsthm,amsfonts}
\usepackage{mathtools} % Bonus
\usepackage{fancyvrb} % This is used to boxed the Verbatim
\usepackage{media9}
\usepackage{epigraph}
\usepackage{smartdiagram}
\usepackage{verbatim}
\usepackage{lmodern}
\usepackage{listings}
\usepackage{chemfig,chemmacros}
\usepackage{fancyvrb}
\usepackage{tcolorbox}
\chemsetup{modules={reactions}}

\setlength{\parindent}{0mm}
\setlength{\parskip}{2ex}
\textheight=22 truecm
\textwidth=16.5 truecm
\oddsidemargin=-0.5cm
\voffset= -0.5 truein
\newcommand{\braket}[2]{\langle #1 | #2 \rangle}
\newcommand{\bra}[1]{\langle #1 |}
\newcommand{\ket}[1]{| #1 \rangle}
\newcommand{\higlight}[1]{\colorbox{yellow}{#1}}
\newcommand{\file}[1]{\textcolor{gray}{\texttt{#1}}}
\newcommand{\note}[1]{\textbf{Note: }\textcolor{red}{#1}}
\newcommand{\spec}[1]{\langle #1 \rangle}
\newcommand{\icomp}{\mathrm{i}}
\newcommand{\trace}{\mathrm{Tr}}
\newcommand{\norm}[1]{\parallel #1 \parallel}
\def\bml#1{\texttt{\color{blue} #1}}  % Bml routines stile for algorithm.
\def\prog#1{\texttt{\color{cyan!90} #1}}  
\def\comm#1{\texttt{\color{gray} #1}} 

\def\saychris#1{\emph{\bf \color{red} (CHRIS: #1)}}
\newenvironment{chris}{\color{red} (CHRIS:)}{\color{black}}

\newenvironment{myshell}{\small \begin{tcolorbox}[colback=black!5!black,colframe=red!75!black] \color{orange!100}                                                                                                                                           
 }{\vspace{-0.8cm}\end{tcolorbox}}
% \newenvironment{myshell}{{\small                                                                                                                                           
%  }{\vspace{-0.8cm}}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{DFTB Based Quantum Molecular Dynamics Simulation using LAMMPS and LATTE: Step-by-step tutorial}
\author{Christian F. A. Negre}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\date{\today}

\begin{document}
  
  \maketitle
  
  \begin{figure}[htpb]
    \centering
    \includegraphics[width=10cm]{./arte.png}
  \end{figure}
  
  \newpage
  
  \section{Introduction}
    
  This notes are intended to give a step-by-step ``howto'' run a Density Functional Tight-Binding (DFTB) based Quantum Molecular Dynamics (\href{https://arxiv.org/abs/1203.6836}{QMD}) calculation using \href{https://github.com/lammps/lammps}{LAMMPS} and \href{https://github.com/lanl/LATTE}{LATTE} computational codes. 
  Large-scale Atomic/Molecular Massively Parallel Simulator (LAMMPS) is an intensively used molecular dynamics code written in C++\cite{PLIMPTON19951}. Los Alamos Transferable Tight-binding for Energetics (LATTE) is a  LANL Fortran code used for quantum chemistry based on the tight-binding method \cite{Sanville2010}. 
  The LAMMPS documentation can be found in \href{http://lammps.sandia.gov/doc/Manual.html}{\texttt{http://lammps.sandia.gov/doc/Manual.html}}. The LATTE documentation can be found in \href{https://github.com/lanl/LATTE/blob/master/Manual/LATTE_manual.pdf}{\texttt{https://github.com/lanl/LATTE/blob/master/Manual/LATTE\_manual.pdf}}.
  With the help of an interface we are now able to drive a molecular dynamics (MD) simulation with LAMMPS using the forces computed at the DFTB level of theory with LATTE. 
  
  Forces are calculated by LATTE from the electronic structure of the system as follows:

 \begin{equation}
    F_k = -tr \left[\rho \frac{\partial H}{\partial R_k} \right] - \frac{1}{2} \sum_i \sum_{j \neq i} q_i q_j \frac{\partial \gamma_{ij}}{\partial R_k} 
    - \frac{\partial E_{pair}}{\partial R_k} 
 \end{equation}

    
  \section{Requirements}
  
  In order to follow this tutorial, we will assume that the reader have a \texttt{LINUX} or \texttt{MAC} operative system with the following packages properly installed: 
  
  \begin{itemize}
   \item  The \verb|git| program for cloning the codes.  
   \item  A \verb|C/C++| compiler (\verb|gcc| and \verb|g++| for example)
   \item  A \verb|Fortran| compiler (\verb|gfortran| for example)
   \item  The LAPACK and BLAS library (GNU \verb|libblas| and \verb|liblapack| for example)
   \item  The \verb|python| interpreter (not essential).
   \item  The \verb|pkgconfig| and \verb|cmake| programs (not essential).
  \end{itemize}
   
  On an \verb|x86_64| GNU/Linux Ubuntu 16.04 distribution the commands to be typed are the following: 
  %

  \begin{myshell}
  \begin{verbatim}
      $ sudo apt-get update   
      $ sudo apt-get --yes --force-yes install gfortran gcc g++
      $ sudo apt-get --yes --force-yes install libblas-dev liblapack-dev   
      $ sudo apt-get --yes --force-yes install cmake pkg-config cmake-data 
      $ sudo apt-get --yes --force-yes install git python             
  \end{verbatim}      
  \end{myshell}
  


  %    
  \textbf{NOTE:} Through the course of this tutorial we will assume that the follower will work and install the programs in the home directory (\verb|$HOME|).

  \section{Download and installation}
  
  \subsection{Download:}
  We will need to clone two essential repositories. One for LAMMPS: 
  %
  \begin{myshell}
   \begin{verbatim}
      $ cd; git clone -b exaalt https://gitlab.com/exaalt/lammps.git
    \end{verbatim}
  \end{myshell}      
  %    
  And another for LATTE:
  %
  \begin{myshell}
   \begin{verbatim}
      $ cd; git clone git@github.com:lanl/LATTE.git
    \end{verbatim}   
  \end{myshell}
  %
  \textbf{NOTE:} Make sure your public ssh key is added to your github profile in order to be able to clone this repositories. 
  Alternative to the git command, these two codes can be Downloaded directly as a \verb|.zip| file from the 
  github webpage clicking on the ``clone or download button'' and choosing the ``download zip'' option. For the case of LAMMPS go to: \href{https://github.com/lammps/lammps}{\texttt{https://github.com/lammps/lammps}} and for LATTE go to: \href{https://github.com/lanl/LATTE}{\texttt{https://github.com/lanl/LATTE}}.
  
  
  \subsection{Compiling LATTE:}  
  The first code that must be compiled is the LATTE code because it has to be compiled as a library so that it can be linked with LAMMPS. In order to compile the LATTE code we proceed as following. We enter the LATTE directory (\verb|cd ~/LATTE|) and modify the \verb|makefile.CHOICES| file according to the operative system, libraries, compiler, etc. There are several examples of \verb|makefile.CHOICES| files inside the 
  makefiles directory (\verb|~/LATTE/makefiles|) that could be used as a template to replace the \verb|makefile.CHOICES| that is located in the main directory. 
  \textbf{IMPORTANT:} Make sure that the \verb|MAKELIB| variable is set ot \verb|ON| (\verb|MAKELIB = ON|) to be able to compile LATTE as a library. 
  
  If you are running on \verb|x86_64| GNU/Linux Ubuntu 16.04 with the packages that we suggested to install, then the actual \verb|makefile.CHOICES| should work. 
  
  To build the code just type make inside the main directory as follows: 
  %
\begin{myshell}
   \begin{verbatim}
      $ cd; cd ~/LATTE
      $ make; make test
    \end{verbatim}
\end{myshell}
  %
  The latter should build the code and test it with some examples that are located in \verb|~/LATTE/tests|.
  A file called \verb|liblatte.a| should appear after building the code. 
  \textbf{NOTE:} Make sure to remove the \verb|liblatte.a| any time the code has to be rebuilt.
  
  \subsection{Compiling LAMMPS:}  
  To build the LAMMPS code and link it with LATTE, got to the LAMMPS folder and enter in the \verb|lib/latte| directory (\verb|cd ~/lammps/lib/latte|). Copy one of the Makefiles to Makefile.lammps as follows:
  \verb|cp Makefile.lammps.gfortran Makefile.lammps| and edit the \verb|Makefile.lammps| file according to your system settings, compilers and LATTE location. There are more instructions in the README file inside \verb|~/lammps/lib/latte|.
  After this is done go to the source folder (\verb|cd ~/lammps/src|) and type the following series of commands: 
  %
\begin{myshell}
   \begin{verbatim}
      $ cd ~/lammps/src
      $ make yes-latte
      $ make yes-molecule 
      $ make serial
    \end{verbatim} 
\end{myshell}
  %
  \subsection{Running an example calculation}
  
  We will assume you have a \verb|pdb/xyz| file containing the coordinates of your system. For the purpose 
  of this tutorial we will use a simple system consisting on eight water molecules in a box. 
  
  We will make a new directory to be able to run so that we can separate the running files from program files. 
  %
  \begin{myshell}   
   \begin{verbatim}
      $ mkdir ~/example_water
      $ cd ~/example_water
      $ cp ~/LATTE/example_lmp/wat_24.pdb . 
    \end{verbatim}
  \end{myshell}  
  %
  Now we need to transform the coordinates to the LAMMPS data file which is formated as follows:
  %
  \begin{myshell}   
   \begin{verbatim}
   
 LAMMPS Description
 
          24 atoms
 
           2 atom types
 
   0.0000000000000000        6.2670000000000003      xlo xhi
   0.0000000000000000        6.2670000000000003      ylo yhi
   0.0000000000000000        6.2670000000000003      zlo zhi
 
 Masses
 
              1   15.994915008544922     
              2   1.0078250169754028     
 
 Atoms
 
    1    1    1   0.0   3.08800   3.70000   3.12400
    2    1    2   0.0   4.05800   3.70000   3.12400
    3    1    2   0.0   2.76400   3.13200   3.84100
    4    1    1   0.0   2.47000   0.39000   1.36000    
    
    \end{verbatim}
  \end{myshell}   
 %    
The format of the file is self explanatory but one can consult the LAMMPS data format for more information: 
\href{http://lammps.sandia.gov/doc/2001/data\_format.html}{\texttt{http://lammps.sandia.gov/doc/2001/data\_format.html}}. For this example we are providing an already formated file which is located in: \\
\verb|~/LATTE/example_lmp/data.wat_24.lmp|

If a particula mass needs to be changed, this has to be done, not only on the .lmp file, but also inside the ``periodic table''

There is a tool that can transform pdb to lmp formats back and forth which is part of the PROGRESS linbrary located in: \href{https://github.com/lanl/qmd-progress}{\texttt{https://github.com/lanl/qmd-progress}}
If installed, a tool can be used to transform coordinates as follows: 
%
  \begin{myshell}
   \begin{verbatim}
      $ ~/qmd-progress/build/changecoords mycoords.pdb mycoords.lmp
    \end{verbatim}   
  \end{myshell}
%
There is another tool in \href{http://open-babel.readthedocs.io/en/latest/FileFormats/The_LAMMPS_data_format.html}{openbabel} library that can convert from any format to the lammps format but it cannot convert back from lammps to any other format. See source \href{https://sourceforge.net/p/openbabel/code/HEAD/tree/openbabel/trunk/src/formats/lmpdatformat.cpp}{code}.  

A general LAMMPS input file will have to be created. 
An example input file can be found in \verb|~/LATTE/example_lmp/in.wat.lmp|. This file has to be copied to the running folder to be used in this tutorial. 
%
  \begin{myshell}
   \begin{verbatim}
      $ cp ~/LATTE/example_lmp/in.wat.lmp ~/example_water/
    \end{verbatim}
  \end{myshell}  
%
For a precise definition of the keywords here used please refer to:\\ \href{http://lammps.sandia.gov/doc/Manual.html}{\texttt{http://lammps.sandia.gov/doc/Manual.html}}.
There are some important points to notice: 

\begin{itemize}
 \item \verb|units| must be set to \verb|metal|
 \item \verb|read_data| must be followed by the name of our data file. In this case this is: \verb|data.wat_24.lmp|.
 \item \verb|atom_modify| must be followed by \verb|sort 0 0.0| in order to avoid an error from resorting the coordinates. 
 \item \verb|fix 2 all latte NULL| will call the LATTE program to compute the forces.
\end{itemize}

  
The following step is to setup the LATTE environment of input files and folders that will allow us to run the LAMMPS-LATTE calculations. For this we can just call a script from the running folder as follows:
%
  \begin{myshell}
   \begin{verbatim}
      $ cd ~/example_water
      $ ~/LATTE/run_latte --setup  
    \end{verbatim}
  \end{myshell}
%
This will automatically make all the necessary folders and input files used by latte. There are two important files that controls the latte programs these are \verb|MDcontroller| and \verb|TBparam/control.in |. A detailed Description of all these keywords can be found in: \\ \href{https://github.com/lanl/LATTE/blob/master/Manual/LATTE_manual.pdf}{\texttt{https://github.com/lanl/LATTE/blob/master/Manual/LATTE\_manual.pdf}} 

An alternative way to provide latte with the necessary keywords is to use a ``latte.in'' file inside the running folder. If this file is present latte will read the keywords from there. We provide and example file 
in \verb|~/LATTE/example_lmp/latte.in|. Copy this file to the running folder as follows:
%
  \begin{myshell}
   \begin{verbatim}
      $ cd ~/example_water
      $ cp ~/LATTE/example_lmp/latte.in .
    \end{verbatim}
  \end{myshell}
%
Finally, to run the calculation we just need to type the following lines: 
%
  \begin{myshell}
   \begin{verbatim}
      $ cd ~/example_water
      $ ~/lammps/src/lmp_serial < in.wat
    \end{verbatim}   
  \end{myshell}

 %      
  \section{Contacts:}
  
  \begin{itemize}
   \item For problems with LAMMPS please contact Steve Plimpton, email: sjplimp@sandia.gov
   \item For problems with LAMMPS-LATTE interface please contact Christian Negre, email: cnegre@lanl.gov
   \item For problems with LATTE please contact Marc Cawkwell, email: cawkwell@lanl.gov
  \end{itemize}
  
  \bibliographystyle{mio.bst}
  \bibliography{MyBib.bib}
  
\end{document}
